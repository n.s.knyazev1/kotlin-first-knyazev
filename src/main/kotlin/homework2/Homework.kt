package homework2

fun main() {
    val lemonadeMl: Int = 18500
    val pinaColadaMl: Short = 200
    val whiskeyMl: Byte = 50
    val freshDrops: Long = 3000000000
    val colaL: Float = 0.5f
    val aleL: Double = 0.666666667
    val somethingCustomTitle: String = "Что-то авторское"

    println("Заказ '$lemonadeMl мл Лимонада' готов!")
    println("Заказ '$pinaColadaMl мл Пинаколады' готов!")
    println("Заказ '$whiskeyMl мл Виски' готов!")
    println("Заказ '$freshDrops капель Фреша' готов!")
    println("Заказ '$colaL л Колы' готов!")
    println("Заказ '$aleL л Эля' готов!")
    println("Заказ '$somethingCustomTitle' готов!")

}