package homework4

fun reverseMath(n: Int): Int {
    var numb: Int = n
    var reversedNum = 0

    while (numb > 0) {
        reversedNum = reversedNum * 10 + numb % 10
        numb /= 10
    }

    return reversedNum
}

fun main() {
    println("Введите число которое нужно перевернуть:")

    val num = readln().toInt()

    println("Перевернутое число: ${reverseMath(num)}")
}

