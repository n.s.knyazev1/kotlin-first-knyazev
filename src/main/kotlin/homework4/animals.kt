package homework4

open class Lion(name: String, height: Int, weight: Int) {
    var favoriteFood: Array<String> = arrayOf("meat","jelly")
    var satietyIndex: Int = 0

    constructor(name: String, height: Int, weight: Int, favoriteFood: Array<String>, satietyIndex: Int) : this(name, height, weight) {
        this.favoriteFood = favoriteFood
        this.satietyIndex = satietyIndex
    }

    open fun eat(food: String) {
        favoriteFood.forEach { if (it == food) satietyIndex++ }
        return println("Lion satiety index is $satietyIndex")
    }
}

class Tiger(name: String, height: Int, weight: Int, favoriteFood: Array<String>, satietyIndex: Int): Lion(name, height, weight, favoriteFood, satietyIndex) {

    override fun eat(food: String) {
        favoriteFood.forEach { if (it == food) satietyIndex++ }
        return println("Tiger satiety index is $satietyIndex")
    }
}

class Behemoth(name: String, height: Int, weight: Int, favoriteFood: Array<String>, satietyIndex: Int): Lion(name, height, weight, favoriteFood, satietyIndex) {

    override fun eat(food: String) {
        favoriteFood.forEach { if (it == food) satietyIndex++ }
        return println("Behemoth satiety index is $satietyIndex")
    }
}

class Wolf(name: String, height: Int, weight: Int, favoriteFood: Array<String>, satietyIndex: Int): Lion(name, height, weight, favoriteFood, satietyIndex) {

    override fun eat(food: String) {
        favoriteFood.forEach { if (it == food) satietyIndex++ }
        return println("Wolf satiety index is $satietyIndex")
    }
}

class Jiraffe(name: String, height: Int, weight: Int, favoriteFood: Array<String>, satietyIndex: Int): Lion(name, height, weight, favoriteFood, satietyIndex) {

    override fun eat(food: String) {
        favoriteFood.forEach { if (it == food) satietyIndex++ }
        return println("Jiraffe satiety index is $satietyIndex")
    }
}

class Elephant(name: String, height: Int, weight: Int, favoriteFood: Array<String>, satietyIndex: Int): Lion(name, height, weight, favoriteFood, satietyIndex) {

    override fun eat(food: String) {
        favoriteFood.forEach { if (it == food) satietyIndex++ }
        return println("Elephant satiety index is $satietyIndex")
    }
}

class Monkey(name: String, height: Int, weight: Int, favoriteFood: Array<String>, satietyIndex: Int): Lion(name, height, weight, favoriteFood, satietyIndex) {

    override fun eat(food: String) {
        favoriteFood.forEach { if (it == food) satietyIndex++ }
        return println("Monkey satiety index is $satietyIndex")
    }
}

class Gorilla(name: String, height: Int, weight: Int, favoriteFood: Array<String>, satietyIndex: Int): Lion(name, height, weight, favoriteFood, satietyIndex) {

    override fun eat(food: String) {
        favoriteFood.forEach { if (it == food) satietyIndex++ }
        return println("Gorilla satiety index is $satietyIndex")
    }
}

fun main(){
    val myLion = Lion("Barsik",12,12)
    val myTiger = Tiger("Tigra",12,12, arrayOf("meat","jelly"),0)
    val myBehemoth = Behemoth("Bigfoot",12,12, arrayOf("grass","alligator"),0)
    val myWolf = Wolf("Woof",12,12, arrayOf("meat","jelly"),0)
    val myJiraffe = Jiraffe("Longneck",12,12, arrayOf("grass","tree"),0)
    val myElephant = Elephant("Raja",12,12, arrayOf("grass","tree"),0)
    val myMonkey = Monkey("Khris",12,12, arrayOf("grass","nuts"),0)
    val myGorilla = Gorilla("Goro",12,12, arrayOf("grass","nuts"),0)

    myLion.eat("meat")
    myTiger.eat("meat")
    myBehemoth.eat("grass")
    myWolf.eat("meat")
    myJiraffe.eat("grass")
    myElephant.eat("grass")
    myMonkey.eat("grass")
    myGorilla.eat("grass")
}