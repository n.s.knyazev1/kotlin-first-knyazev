package intensive2

import java.text.DecimalFormat

val format: (Double) -> String = { item ->
    DecimalFormat("#.##").format(item)
}

val modify: (List<Double?>) -> Double = { list ->

    list
        .asSequence()
        .filterNotNull()
        .take(10)
        .sortedDescending()
        //кажется у того кто придумал задачу от сортировки слагаемых сумма меняется :D
        .map {
            if ( it.toInt() % 2 == 0 ) {
                it * it
            } else {
                it / 2
            }
        }
        .filter { it < 25 }
        .reduce { sum, item -> sum + item }
}

fun main() {
    val list1 = listOf(13.31, 3.98, 12.0, 2.99, 9.0)
    val list2 = listOf(133.21, null, 233.98, null, 26.99, 5.0, 7.0, 9.0)

    println(format(modify(list1)))
    println(format(modify(list2)))
}