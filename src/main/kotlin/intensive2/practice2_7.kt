package intensive2

val sum = { a: Double, b: Double -> a + b }
val subtraction = { a: Double, b: Double -> a - b }
val multiply = {a: Double, b: Double -> a * b }
val divide = {a: Double, b: Double -> (if (b.toInt() != 0) a / b else "Ошибка! Делить на 0 нельзя (только если ты не кандидат математических наук).") }
//тип значений на выходе изменил, но я подумал что на результат не должно повлиять
//есть ощущение что это проще делается, но я не понимаю как)
fun calculator(lambda: (Double, Double) -> Any, a: Double, b: Double ) {
    println(lambda(a,b))
}

fun getCalculationMethod(name: String) : (Double, Double) -> Any {
    return when (name) {
        "+" -> sum
        "-" -> subtraction
        "*" -> multiply
        "/" -> divide
        else -> throw UnsupportedOperationException()
    }
}
fun main() {
    calculator(getCalculationMethod("+"), 2.0, 3.0)
    calculator(getCalculationMethod("-"), 2.0, 13.0)
    calculator(getCalculationMethod("*"), 2.0, 13.0)
    calculator(getCalculationMethod("/"), 2.0, 13.0)
    calculator(getCalculationMethod("/"), 2.0, 0.0)
}
