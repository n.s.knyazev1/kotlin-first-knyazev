package intensive2

val validateInn: (String) -> Boolean = { inn ->
    val coefficients10 = listOf(2, 4, 10, 3, 5, 9, 4, 6, 8)
    val coefficients121 = listOf(7, 2, 4, 10, 3, 5, 9, 4, 6, 8)
    val coefficients122 = listOf(3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8)

    when (inn.length) {
        10 -> {
            val controlDigit = inn.take(9)
                .mapIndexed { index, char -> char.digitToInt() * coefficients10[index] }
                .sum() % 11 % 10

            val lastDigit = inn[9].toString().toInt()

            controlDigit == lastDigit
        }

        12 -> {
            val controlDigit1 = inn.take(10)
                .mapIndexed { index, char -> char.digitToInt() * coefficients121[index] }
                .sum() % 11 % 10

            val controlDigit2 = inn.take(11)
                .mapIndexed { index, char -> char.digitToInt() * coefficients122[index] }
                .sum() % 11 % 10

            val lastDigits = inn.takeLast(2).map { it.digitToInt() }

            controlDigit1 == lastDigits[0] && controlDigit2 == lastDigits[1]
        }
        else -> false
    }
}

fun validateInnFunction(inn: String, function: (String) -> Boolean) {
    println("ИНН $inn ${if (function(inn)) "валиден" else "не валиден"}")
}

fun main() {
    val inn = "707574944998"
    val inn1 = "3453456346"
    validateInnFunction(inn, validateInn)
    validateInnFunction(inn1, validateInn)
}