package intensive2

abstract class Pet

interface Runnable {
    fun run()
}

interface Swimmable {
    fun swim()
}

open class Cat : Pet(), Runnable, Swimmable {
    override fun run() {
        println("I am a Cat, and I'm running")
    }

    override fun swim() {
        println("I am a Cat, and I'm swimming")
    }
}

open class Fish : Pet(), Swimmable {
    override fun swim() {
        println("I am a Fish, and I'm swimming")
    }
}

class Manul : Cat() {
    override fun run() {
        println("I am a Manul, and I'm running")
    }
}

class Cheetah : Cat() {
    override fun run() {
        println("I am a Cheetah, and I'm running")
    }
}

class Fugu : Fish() {
    override fun swim() {
        println("I am a Fugu, and I'm swimming")
    }
}

class Stonefish : Fish() {
    override fun swim() {
        println("I am a Stonefish, and I'm swimming")
    }
}

fun <T : Runnable> useRunSkill(obj: T) {
    obj.run()
}

fun <T : Swimmable> useSwimSkill(obj: T) {
    obj.swim()
}

fun <T> useSwimAndRunSkill(obj: T) where T : Runnable, T : Swimmable {
    obj.run()
    obj.swim()
}

fun main() {
    val cat = Cat()
    val fish = Fish()
    val manul = Manul()
    val cheetah = Cheetah()
    val fugu = Fugu()
    val stonefish = Stonefish()

    useRunSkill(cat)
    useSwimSkill(fish)
    useSwimAndRunSkill(manul)
    useSwimAndRunSkill(cheetah)
    useSwimSkill(fugu)
    useSwimSkill(stonefish)
}