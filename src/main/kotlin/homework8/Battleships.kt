package homework8

import java.util.Random

class Ship(val size: Int) {
    var location: MutableList<Pair<Int, Int>> = mutableListOf()
}

class Player(val name: String) {
    val battlefield: Array<Array<Char>> = Array(4) { Array(4) { '0' } }
    val ships: MutableList<Ship> = mutableListOf()

    fun initializeShips() {
        // 2 корабля по 1 клетке
        for (i in 1..2) {
            val ship = Ship(1)
            ships.add(ship)
        }

        // 1 корабль 2 клетки
        val ship = Ship(2)
        ships.add(ship)
    }

    fun placeShips() {
        for (ship in ships) {
            var isShipPlaced = false
            while (!isShipPlaced) {
                val row = Random().nextInt(4)
                val col = Random().nextInt(4)
                val orientation = Random().nextBoolean() // true для горизонтальной, false для вертикальной

                if (isShipFit(ship, row, col, orientation)) {
                    // размещение корабля
                    for (i in 0 until ship.size) {
                        if (orientation) {
                            battlefield[row][col + i] = '1' + ship.size - 1 // '1' или '2' в зависимости от корабля
                            ship.location.add(Pair(row, col + i))
                        } else {
                            battlefield[row + i][col] = '1' + ship.size - 1
                            ship.location.add(Pair(row + i, col))
                        }
                    }
                    isShipPlaced = true
                }
            }
        }
    }

    private fun isShipFit(ship: Ship, row: Int, col: Int, orientation: Boolean): Boolean {
        if (orientation && col + ship.size <= 4) {
            for (i in 0 until ship.size) {
                if (battlefield[row][col + i] != '0') {
                    return false
                }
            }
            return true
        } else if (!orientation && row + ship.size <= 4) {
            for (i in 0 until ship.size) {
                if (battlefield[row + i][col] != '0') {
                    return false
                }
            }
            return true
        }
        return false
    }

    //отображение поля для отладки
    fun displayBattlefield() {
        println("\n${name}'s Battlefield:")

        for (row in battlefield) {
            for (cell in row) {
                print("$cell ")
            }
            println()
        }
    }
}

fun main() {
    val players: MutableList<Player> = mutableListOf()

    println("Enter homework8.Player 1 Name:")
    val player1Name = readLine() ?: ""
    val player1 = Player(player1Name)

    println("Enter homework8.Player 2 Name:")
    val player2Name = readLine() ?: ""
    val player2 = Player(player2Name)

    players.add(player1)
    players.add(player2)

// инициализировать и разместить корабли для игроков
    for (player in players) {
        player.initializeShips()
        player.placeShips()
    }

    var currentPlayerIndex = 0

    while (true) {
        val currentPlayer = players[currentPlayerIndex]
        val opponentPlayer = players[(currentPlayerIndex + 1) % 2]

        currentPlayer.displayBattlefield()

        println("\n${currentPlayer.name}, it's your turn.")
        var isValidInput = false
        var row = 0
        var col = 0

        while (!isValidInput) {
            println("Enter the row (1-4):")
            val rowInput = readLine()?.toIntOrNull()
            println("Enter the column (1-4):")
            val colInput = readLine()?.toIntOrNull()

            if (rowInput in 1..4 && colInput in 1..4) {
                row = rowInput!! - 1
                col = colInput!! - 1
                isValidInput = true
            } else {
                println("Invalid input! Please try again.")
            }
        }

        val cell = opponentPlayer.battlefield[row][col]
        println("Result:")

        if (cell in '1'..'2') {
            println("Hit!")
            opponentPlayer.battlefield[row][col] = 'H'
            // проверяем уничтожен ли корабль
            var isShipDestroyed = true
            for (ship in opponentPlayer.ships) {
                if (ship.location.contains(Pair(row, col))) {
                    ship.location.remove(Pair(row, col))
                    if (ship.location.isNotEmpty()) {
                        isShipDestroyed = false
                    }
                    break
                }
            }

            if (isShipDestroyed) {
                println("homework8.Ship Destroyed!")
            }

            // проверяем уничтожены ли все корабли
            var isAllShipsDestroyed = true
            for (ship in opponentPlayer.ships) {
                if (ship.location.isNotEmpty()) {
                    isAllShipsDestroyed = false
                    break
                }
            }

            if (isAllShipsDestroyed) {
                println("Congratulations, ${currentPlayer.name}! You won!")
                break
            }

            // игрок получает дополнительный ход при попадании
            continue
        } else if (cell == 'H' || cell == 'M') {
            println("Already hit or missed this cell!")
        } else {
            println("Miss!")
            opponentPlayer.battlefield[row][col] = 'M'
        }

        // переключение ходов
        currentPlayerIndex = (currentPlayerIndex + 1) % 2
    }
}

//решение такое себе, понял что можно было как-то через map сделать
//но понял слишком поздно, может перепишу