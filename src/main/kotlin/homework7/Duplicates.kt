package homework7
fun removeDuplicates(list: List<Int>): List<Int> {
    return list.distinct()
}

fun main() {
    val originalList = listOf(1,3,3,5,2,3,5,5,3,3,5,6,3,5,6,6,3)
    val uniqueList = removeDuplicates(originalList)
    println(uniqueList)
}