package homework7

class ShoppingCart {

    private val prices = mutableMapOf(
        "potato" to 33.0,
        "sugar" to 67.5,
        "milk" to 58.7,
        "cereal" to 78.4,
        "onion" to 23.76,
        "tomato" to 88.0,
        "cucumber" to 68.4,
        "bread" to 22.0
    )

    private val discountSet = setOf("milk", "bread", "sugar")
    private val discountValue = 0.20
    private val vegetableSet = setOf("potato", "tomato", "onion", "cucumber")
    private val cartItems = mutableMapOf("potato" to 2, "cereal" to 2, "milk" to 1, "sugar" to 3, "onion" to 1, "tomato" to 2, "cucumber" to 2, "bread" to 3)

    fun countVegetables(): Int {
        var counter = 0

        for (item in cartItems) {
            if (vegetableSet.contains(item.key)) {
                counter += 1
            }
        }

        return counter
    }

    fun calculateTotalCost(): Double {
        var totalCost = 0.0

        for ((item, quantity) in cartItems) {
            val price = prices[item] ?: continue

            totalCost += if (discountSet.contains(item)) {
                val discountedPrice = price * (1 - discountValue)
                discountedPrice * quantity
            } else {
                price * quantity
            }
        }

        return totalCost
    }
}

fun main() {
    val shoppingCart = ShoppingCart()

    val vegetableCount = shoppingCart.countVegetables()
    println("Количество овощей в корзине: $vegetableCount")

    val totalCost = shoppingCart.calculateTotalCost()
    println("Итоговая стоимость всех товаров в корзине: $totalCost")
}