package homework6

class User(login: String, password: String, passwordConfirm: String)

open class WrongLoginException : Exception()

open class WrongPasswordException : Exception()

open class WrongPasswordConfirmException : Exception()
//не понял как нормально вывести с двумя классами исключений по условиям задачи, поэтому добавил третий

fun auth(login: String, password: String, passwordConfirm: String): User {

    if (login.length !in 1..20) {
        throw WrongLoginException()
    }

    if (password.length < 10) {
        throw WrongPasswordException()
    }

    if (passwordConfirm != password) {
        throw WrongPasswordConfirmException()
    }

    return User(login, password, passwordConfirm)
}

fun main() {
    try {
        println("Введите логин: ")
        val login: String = readln()

        println("Введите пароль: ")
        val password: String = readln()

        println("Введите подтверждение пароля: ")
        val passwordConfirm: String = readln()

        auth(login, password, passwordConfirm)

    } catch (e: WrongPasswordConfirmException) {
        println("Пароль и подтверждение пароля должны совпадать")
    } catch (e: WrongPasswordException) {
        println("В пароле не должно быть менее 10 символов")
    } catch (e: WrongLoginException) {
        println("В логине не должно быть более 20 символов")
    }

}