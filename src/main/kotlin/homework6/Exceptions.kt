open class CustomException(s: String) : Exception("Custom Exception")
open class CustomException1(s: String) : CustomException("CustomException1")
open class CustomException2 : CustomException1("CustomException2")

fun exceptionTest(n: Int) {
    when (n) {
        1 -> throw CustomException2()
        2 -> throw CustomException1("CustomException2")
        3 -> throw CustomException("CustomException1")
        else -> throw Exception("Exception")
    }
}

fun main() {
    try {
        exceptionTest(readln().toInt())
    } catch (e: CustomException2) {
        println("CustomException2: " + e.message)
    } catch (e: CustomException1) {
        println("CustomException1: " + e.message)
    } catch (e: CustomException) {
        println("CustomException: " + e.message)
    } catch (e: Exception) {
        println("Exception: " + e.message)
    }
}