package homework5


abstract class Animal: Feedable {
    abstract val name: String
    abstract val height: Int
    abstract val weight: Int
    abstract val favoriteFood: Array<String>
    abstract val satietyIndex: Int
}

interface Feedable {
    fun feed(food: Array<String>)
}

class Lion(
    override val name: String,
    override val height: Int,
    override val weight: Int
): Animal() {
    override val favoriteFood: Array<String> = arrayOf("meat", "jelly")
    override var satietyIndex: Int = 0

    override fun feed(food: Array<String>) {
        println("Лев покормлен!")
    }
}

class Tiger(
    override val name: String,
    override val height: Int,
    override val weight: Int,
): Animal() {
    override val favoriteFood: Array<String> = arrayOf("meat", "jelly")
    override var satietyIndex: Int = 0

    override fun feed(food: Array<String>) {
        println("Тигр покормлен!")
    }
}

class Behemoth(
    override val name: String,
    override val height: Int,
    override val weight: Int,
): Animal() {
    override val favoriteFood: Array<String> = arrayOf("grass", "fish")
    override var satietyIndex: Int = 0

    override fun feed(food: Array<String>) {
        println("Бегемот покормлен!")
    }
}

class Wolf(
    override val name: String,
    override val height: Int,
    override val weight: Int,
): Animal() {
    override val favoriteFood: Array<String> = arrayOf("meat", "jelly")
    override var satietyIndex: Int = 0

    override fun feed(food: Array<String>) {
        println("Волк покормлен!")
    }
}

class Jiraffe(
    override val name: String,
    override val height: Int,
    override val weight: Int,
): Animal() {
    override val favoriteFood: Array<String> = arrayOf("grass", "nuts")
    override var satietyIndex: Int = 0

    override fun feed(food: Array<String>) {
        println("Жираф покормлен!")
    }
}

class Elephant(
    override val name: String,
    override val height: Int,
    override val weight: Int,
): Animal() {
    override val favoriteFood: Array<String> = arrayOf("grass", "jelly")
    override var satietyIndex: Int = 0

    override fun feed(food: Array<String>) {
        println("Слон покормлен!")
    }
}

class Monkey(
    override val name: String,
    override val height: Int,
    override val weight: Int,
): Animal() {
    override val favoriteFood: Array<String> = arrayOf("grass", "nuts")
    override var satietyIndex: Int = 0

    override fun feed(food: Array<String>) {
        println("Шимпанзе покормлен!")
    }
}

class Gorilla(
    override val name: String,
    override val height: Int,
    override val weight: Int,
): Animal() {
    override val favoriteFood: Array<String> = arrayOf("grass", "nuts")
    override var satietyIndex: Int = 0

    override fun feed(food: Array<String>) {
        println("Горилла покормлен!")
    }
}

fun feedAll(food: Array<String>, animals: Array<Feedable>) {

    for (animal in animals) {
        animal.feed(food)
    }
}

fun main() {

    val animals: Array<Feedable> = arrayOf(
        Lion("Barsik",30,30),
        Tiger("Tigra", 10,10),
        Behemoth("Boris",100, weight = 900),
        Wolf("Volchiara",20,30),
        Jiraffe("Eugene",300,100),
        Elephant("Raja",200,900),
        Monkey("Lola",30,10),
        Gorilla("Dexter",180,100)
    )
    val foods = arrayOf("jelly", "meat", "potato","grass","nuts")

    feedAll(foods, animals)
}