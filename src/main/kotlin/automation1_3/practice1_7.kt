package automation1_3

fun arguments(vararg params: String) {
    var stringArgs = ""

    params.forEach { stringArgs+=("$it;") }
    println("Передано ${params.count()} аргументов: $stringArgs")
}

fun main(){
    arguments("ichi", "ni", "san", "yon", "go")
}