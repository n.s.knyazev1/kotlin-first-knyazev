package automation1_3

data class Person(
    val name: String,
    val surname: String,
    val fathername: String? = null,
    val gender: String,
    val birthdate: String,
    val age: Int,
    val inn: String? = null,
    val snils: String? = null)

fun main() {
    val van = Person("Van", "Darkholm", "Ivanovich", "male", "1987-01-01", 39)

    println(van.toString())
}