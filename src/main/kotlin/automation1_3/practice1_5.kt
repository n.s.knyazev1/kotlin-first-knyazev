package automation1_3

fun createUser(
    name: String,
    surname: String,
    fathername: String? = null,
    gender: String,
    birthdate: String,
    age: Int,
    inn: String? = null,
    snils: String? = null
) {
    println("name = $name, surname = $surname" +
            "${if (!fathername.isNullOrBlank()) ", fathername = $fathername" else ""}" +
            ", gender = $gender, birthdate = $birthdate, age = $age" +
            "${if (!inn.isNullOrBlank()) ", inn = $inn" else ""}" +
            "${if (!snils.isNullOrBlank()) ", snils = $snils" else ""}")
}

fun main() {
    createUser(name = "Ivan", surname = "Vasilevich", gender = "male", birthdate = "12-12-1993", age = 39)
    createUser(age = 39, birthdate = "04-03-1984", surname = "Kim", name = "Vladlen", gender = "helicopter", fathername = "Vladislavovich", inn = "6666666666", snils = "666-666-666")
}