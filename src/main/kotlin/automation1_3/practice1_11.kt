package automation1_3

class Companion {
    companion object MyCompanion{
        private var count: Int = 0
        fun counter() {
            count+=1
            return println("Вызван counter. Количество вызовов = $count")
        }
    }
}

fun main() {
    Companion.counter()
    Companion.counter()
}