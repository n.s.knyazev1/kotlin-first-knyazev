package automation1_3

fun MutableList<Int>.square(): List<Int> { return this.map { it*it } }

fun main() {
    val myMutableList = mutableListOf(1,4,9,16,25)
    println(myMutableList.square())
}