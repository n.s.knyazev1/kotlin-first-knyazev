package automation1_3

import java.text.DecimalFormat
import java.time.LocalDate
import java.util.logging.Handler

fun Double.roundTo2DecimalPlaces(): String {
    return DecimalFormat("#.##").format(this)
}
fun typeCasting(anything: Any?) {
    val foundationDate = LocalDate.of(2006, 12, 24)

    if (anything == null) {
        println("Объект равен null")
    } else {
        when (anything) {
            is String -> println("Я получил тип String = '$anything', ее длина равна ${anything.length}")
            is Int -> println("Я получил тип Int = '$anything', его квадрат равен ${anything*anything}")
            is Double -> {
                val formatted = anything.roundTo2DecimalPlaces()
                println("Я получил тип Double = '$anything', это число округляется до ${formatted}")
            }
            is LocalDate -> {
                when {
                    anything > foundationDate -> println("Я получил LocalDate = '$anything', эта дата больше чем дата основания Tinkoff")
                    anything == foundationDate -> println("Я получил LocalDate = '$anything', это дата основания Tinkoff")
                    anything < foundationDate -> println("Я получил LocalDate = '$anything', эта дата меньше чем дата основания Tinkoff")
                }
            }
            else -> println("Неизвестный тип")
        }
    }
}

fun main() {
    typeCasting("Privet")
    typeCasting(145)
    typeCasting(145.0)
    typeCasting(145.2817812)
    typeCasting(LocalDate.of(1990,1,1))
    typeCasting(null)
    typeCasting(Handler::class)
}