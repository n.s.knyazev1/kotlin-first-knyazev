package homework3
/*

Елисей — студент 2-го курса политеха. Он получил практическое задание по информатике: нужно отсортировать массив по возрастанию.
По закону подлости он как раз пропустил несколько лекций на эту тему.

Елисей пошел искать решение в интернете и, не разобравшись, выбрал первый попавшийся вариант.

Так совпало, что Елисей, изучает Kotlin, как и вы. Помогите ему разобраться в коде и исправьте ошибки в алгоритме сортировки.

Что нужно сделать:

У вас есть код, который Елисей взял из интернета:

fun main() {
   val myArray = arrayOf(1, -1, -2, 4, 7, 10, 0, 19, -27)

   for(i in 1.. myArray.size - 2) {
       var isSwapped = false
       for(j in 0 until myArray.size - 1 - i) {
           if(myArray[j] < myArray[j+1]) {
               val swap = myArray[j]
               myArray[j] = myArray[j+1]
               myArray[j+1] = swap
               isSwapped = true
           }
       }
       if (!isSwapped) break
   }

   for (item in myArray) {
       println(item)
   }
}
Исправьте его так, чтобы массив целых чисел myArray сортировался по возрастанию.

*/

fun main() {
    val myArray = arrayOf(1, -1, -2, 4, 7, 10, 0, 19, -27)

    for(i in 0 until myArray.size) {
        var isSwapped = false
        for(j in 0 until myArray.size - 1 - i) {
            if(myArray[j] > myArray[j+1]) {
                val swap = myArray[j]
                myArray[j] = myArray[j+1]
                myArray[j+1] = swap
                isSwapped = true
            }
        }
        if (!isSwapped) break
    }

    for (item in myArray) {
        println(item)
    }

}

/*

я бы конечно лучше вот так сделал, но боюсь скажут что задача была не в этом

fun main() {

    val myArray = arrayOf(1, -1, -2, 4, 7, 10, 0, 19, -27)

    println(myArray.sorted())

}

*/