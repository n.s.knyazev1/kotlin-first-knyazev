package homework3

import java.text.DecimalFormat
/*

Татьяна Александровна — классный руководитель 11 “Б” класса.
На носу ЕГЭ по математике, и она хочет посчитать, сколько в ее классе отличников, хорошистов, троечников и двоечников.
Математика — ее профильный предмет, и, как настоящий математик, она решила посчитать количество учеников разной степени успеваемости в процентах.
Учеников у нее много, ровно как и работы, а дело срочное.
Помогите Татьяне Александровне справиться с поставленной задачей!

Что нужно сделать:
У вас есть журнал успеваемости (массив) с оценками (целыми числами). Посчитайте, сколько % из класса — отличники, хорошисты, троечники и двоечники.
Полученные результаты выведите на экран в следующем формате: “Отличников - 25.8%”.

*/
fun formatResult(number: Double): String {
    val df = DecimalFormat("0.00")
    return df.format(number).replace("\\.00$".toRegex(), "")
}
fun main() {
    val journal = arrayOf(2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3,4,5,2,3,4,5)
    var countOfTwos: Int = 0
    var countOfThrees: Int = 0
    var countOfFours: Int = 0
    var countOfFives: Int = 0

    journal.forEach {
        when(it) {
            2 -> countOfTwos += 1
            3 -> countOfThrees += 1
            4 -> countOfFours += 1
            5 -> countOfFives += 1
        }
    }

    var percentOfTwos: Double = (countOfTwos.toFloat() / journal.size) * 100.0
    val percentOfThrees: Double = (countOfThrees.toFloat() / journal.size) * 100.0
    val percentOfFours: Double = (countOfFours.toFloat() / journal.size) * 100.0
    val percentOfFives: Double = (countOfFives.toFloat() / journal.size) * 100.0

    val formattedPercentOfTwos = formatResult(percentOfTwos)
    val formattedPercentOfThrees = formatResult(percentOfThrees)
    val formattedPercentOfFours = formatResult(percentOfFours)
    val formattedPercentOfFives = formatResult(percentOfFives)
//получилось нормально вывести
    println("Двоечников: $formattedPercentOfTwos%\nТроечников: $formattedPercentOfThrees%\nХорошистов: $formattedPercentOfFours%\nОтличников: $formattedPercentOfFives%")

}